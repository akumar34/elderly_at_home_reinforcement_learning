import tensorflow
from keras.models import Sequential
from keras.layers import Dense
from keras.optimizers import Adam
import random, copy
import numpy as np
import re

import elderlyhelper

def get_state(user_action, last_agent_action):
	user_features = elderlyhelper.FeatureUtils.transform_to_one_hot_encoding(user_action['request'],elderlyhelper.FeatureUtils.request_feature_extractor)
	if last_agent_action:
		last_agent_features = elderlyhelper.FeatureUtils.transform_to_one_hot_encoding(last_agent_action['request'],elderlyhelper.FeatureUtils.request_feature_extractor)
	else:
		last_agent_features = np.zeros((1,elderlyhelper.FeatureUtils.request_feature_extractor.get_feature_names().shape[0]), dtype=object)
	metadata_features = elderlyhelper.FeatureUtils.transform_to_one_hot_encoding(user_action['metadata'],elderlyhelper.FeatureUtils.metadata_feature_extractor)
	state_representation=np.hstack((metadata_features,user_features,last_agent_features))
	return state_representation.reshape(-1)

#user_action={'metadata':[0,'mediumconf'],'request':['user','rn','cup','box',]},last_agent_action={'metadata':[0,'mediumconf'],'request':['agent','qyn','cup','box',0]}

def predict(user_action, last_agent_action, model):
	s = get_state(user_action, last_agent_action).reshape(1,61)
	q_values = model.predict(s).flatten()
	for i,action in enumerate(elderlyhelper.DialogueUtils.AGENT_ACTIONS):
		print(i,action,q_values[i])
	index = np.argmax(q_values)
	print("Best qvalue",index,elderlyhelper.DialogueUtils.AGENT_ACTIONS[index])

def _build_model():
	model = Sequential()
	model.add(Dense(80, input_dim=61, activation='relu'))
	model.add(Dense(25, activation='linear'))
	model.compile(loss='mse', optimizer=Adam(lr=1e-3))
	return model

load_weights_file_path='saved_models/model.h5'
beh_load_file_path = re.sub(r'\.h5', r'_beh.h5', load_weights_file_path)
beh_model=_build_model()
beh_model.load_weights(beh_load_file_path)

predict(user_action={'metadata':[3,'mediumconf'],'request':['user','ry','ball','cabinet',0]},\
	last_agent_action={'metadata':[3,'mediumconf'],'request':['agent','qyn','ball','cabinet',0]},model=beh_model)
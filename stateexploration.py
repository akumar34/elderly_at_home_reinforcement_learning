import pickle
import os
import elderlyhelper
os.remove('test_space_exploration.txt')
f=open("test_space_exploration.txt","w")
f.close()
#state_space_freq=elderlyhelper.DQNAgent.get_state_space_freq()
#dm={k: v for k, v in sorted(state_space_freq.items(), key=lambda item: -item[1])}

#for k,v in dm.items():
#	print("action",k,"freq",v)

#stat_freq=elderlyhelper.DQNAgent.get_stat_freq()
#dm={k: v for k, v in sorted(stat_freq.items(), key=lambda item: -item[1])}

#pickle.dump(state_space_freq,open('state_space_freq.pkl','wb'))
#pickle.dump(stat_freq,open('stat_freq.pkl','wb'))

stat_freq=pickle.load(open('stat_freq.pkl','rb'))
state_space_freq=pickle.load(open('state_space_freq.pkl','rb'))

metadata_size=elderlyhelper.FeatureUtils.metadata_feature_extractor.get_feature_names().shape[0]
request_size=elderlyhelper.FeatureUtils.request_feature_extractor.get_feature_names().shape[0]

with open('test_space_exploration.txt','a+') as f:
	for k,v in stat_freq.items():
		f.write("\n...START...\n")
		f.write("INFORM " + str(elderlyhelper.FeatureUtils.inverse_transform_to_feature_vector(\
			list(k)[:metadata_size],elderlyhelper.FeatureUtils.metadata_feature_extractor)))
		f.write("\n")
		f.write("USER " + str(elderlyhelper.FeatureUtils.inverse_transform_to_feature_vector(\
			list(k)[metadata_size:request_size+metadata_size],elderlyhelper.FeatureUtils.request_feature_extractor)))
		f.write("\n")
		f.write("AGENT " + str(elderlyhelper.FeatureUtils.inverse_transform_to_feature_vector(\
			list(k)[request_size+metadata_size:],elderlyhelper.FeatureUtils.request_feature_extractor)))
		f.write("\n")
		f.write("Frequency " + str(v))
		f.write("\n...END...\n")
		f.write("\n\n")

from .dialogueutils import DialogueUtils
from .featureutils import FeatureUtils
from .parameters import Parameters
import random, copy
import numpy as np

class User:
	#the constants are the hyperparams in constants.json file
	def __init__(self):
		self.max_turns = Parameters.MAX_TURNS

	def reset(self):
		self.state={}
		self.state['metadata']=[]
		self.state['request']=[]
		user_response = self._return_response()

		print("\n")
		print("Init user action: {}".format(user_response))
		print("\n")

		return user_response

	def _return_response(self):
		"""
		Asks user in console for response then receives a response as input.
		Format must be like this:
			instr/turns:0,slot_similarity:0.4,speech_confidence:0.6/object:cup,location:cabinet
		intents, informs keys and values, and request keys and values cannot contain

		Returns:
		dict: The response of the user
		"""

		user_response = {"metadata": np.zeros(FeatureUtils.AUDIBLE_CONFIDENCE_LEVEL_IDX+1, dtype=object), \
			"request": np.zeros(FeatureUtils.CURRENT_SLOT_IDX+1, dtype=object)}
		while True:
			input_string = input('Response: ')

			chunks = input_string.split('/')
			metadata_correct = True
			if len(chunks[0]) > 0:
				metadata_items_list = chunks[0].split(', ')
				for inf in metadata_items_list:
					inf = inf.split(':')
					if inf[0] not in DialogueUtils.METADATA_FEATURES_DESC:
						metadata_correct = False
						break
					if inf[0] == 'turn':
						user_response['metadata'][FeatureUtils.TURNS_IDX] = inf[1]
					elif inf[0] == 'conf':
						user_response['metadata'][FeatureUtils.AUDIBLE_CONFIDENCE_LEVEL_IDX] = inf[1]

			request_correct = True
			if len(chunks[2]) > 0:
				requests_key_list = chunks[2].split(', ')
				for req in requests_key_list:
					req = req.split(':')
					if req[0] not in DialogueUtils.REQUEST_FEATURES_DESC:
						request_correct = False
						break
					if req[0] == 'speaker':
						user_response['request'][FeatureUtils.SPEAKER_IDX] = req[1]
					elif req[0] == 'intent':
						user_response['request'][FeatureUtils.INTENT_IDX] = req[1]
					elif req[0] == 'object':
						if req[1] == 'None':
							req[1] = None
						user_response['request'][FeatureUtils.OBJECT_SLOT_IDX] = req[1]
					elif req[0] == 'location':
						if req[1] == 'None':
							req[1] = None
						user_response['request'][FeatureUtils.LOCATION_SLOT_IDX] = req[1]
					elif req[0] == 'idx':
						user_response['request'][FeatureUtils.CURRENT_SLOT_IDX] = int(req[1])
			if metadata_correct and request_correct:
				break

		return user_response

	def _return_success(self):
		success = -2
		while success not in (-1,0,1):
			success = int(input('Sucess?: '))
		return success

		#Example:
		#  Response: //speaker:user, intent:instr, object:cup, location:cabinet, idx:0
		#  Response: //speaker:user, intent:ry, object:cup, location:cabinet, idx:0
		#  Response: //speaker:user, intent:ry, object:cup, location:cabinet, idx:1
		#  Response: //speaker:user, intent:done, object:None, location:None, idx:2
	def step(self, agent_action):
		print("\n")
		print("Agent action: {}".format(agent_action))
		print("\n")

		user_response = {"metadata": np.zeros(FeatureUtils.AUDIBLE_CONFIDENCE_LEVEL_IDX+1, dtype=object), \
		"request": np.zeros(FeatureUtils.CURRENT_SLOT_IDX+1, dtype=object)}

		status=DialogueUtils.NO_OUTCOME
		done=False
		if agent_action['metadata'][FeatureUtils.TURNS_IDX] >= Parameters.MAX_TURNS:
			status=DialogueUtils.FAIL
			
			user_response['request'][FeatureUtils.SPEAKER_IDX]='user'
			user_response['request'][FeatureUtils.INTENT_IDX]=DialogueUtils.TERMINAL_INTENT[0]
			user_response['request'][FeatureUtils.OBJECT_SLOT_IDX] = None
			user_response['request'][FeatureUtils.LOCATION_SLOT_IDX] = None
			user_response['request'][FeatureUtils.CURRENT_SLOT_IDX] = 2
		else:
			user_response = self._return_response()
			status = self._return_success()

		if status == DialogueUtils.FAIL or status == DialogueUtils.SUCCESS:
			done = True

		if not user_response['metadata']:
			user_response['metadata']=np.zeros(FeatureUtils.AUDIBLE_CONFIDENCE_LEVEL_IDX+1, dtype=object)
			user_response['metadata'][FeatureUtils.TURNS_IDX]=agent_action['metadata'][FeatureUtils.TURNS_IDX]
			user_response['metadata'][FeatureUtils.AUDIBLE_CONFIDENCE_LEVEL_IDX]=\
				agent_action['metadata'][FeatureUtils.AUDIBLE_CONFIDENCE_LEVEL_IDX]

		reward = DialogueUtils.reward_function(done, user_response['metadata'][FeatureUtils.AUDIBLE_CONFIDENCE_LEVEL_IDX],\
			user_response['metadata'][FeatureUtils.TURNS_IDX], status)

		print("\n")
		print("UserSim response", user_response,"Episode status",status,"Episode done",done,"Episode reward",reward)
		print("\n")
		return user_response, reward, done, True if status is 1 else False

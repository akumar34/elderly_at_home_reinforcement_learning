from .parameters import Parameters
import copy
import random
import numpy as np

class DialogueUtils:
	#elderly is agent
	AGENT_INTENTS=['chk','qyn']

	TERMINATE_INTENT=['done']

	#helper is user
	USER_INTENTS=['rn','ry','rw','instr']

	ALL_INTENTS = AGENT_INTENTS + TERMINATE_INTENT + USER_INTENTS

	SPEAKERS=['user','agent']

	# At the end of an episode, we determine whether it was a successful episode or failed episode.
	FAIL = -1
	NO_OUTCOME = 0
	SUCCESS = 1

	REQUEST_SLOTS=['object','location']

	OBJECT_SLOT_VALUES=['ball','cup']
	LOCATION_SLOT_VALUES=['box','drawer','cabinet']

	AUDIBLE_CONFIDENCE_LEVEL=['lowconf','mediumconf','highconf']

	METADATA_FEATURES_DESC=['turn','conf']
	REQUEST_FEATURES_DESC=['speaker','intent','object','location','idx']

	USER_GOALS=[]
	for object_slot in OBJECT_SLOT_VALUES:
		for location_slot in LOCATION_SLOT_VALUES:
			if not object_slot:
				continue
			USER_GOALS.append([object_slot, location_slot])

	@staticmethod
	def generate_goal():
		GOAL = random.choice(DialogueUtils.USER_GOALS)
		if random.random() < 0.3:
			return GOAL, GOAL
		else:
			return GOAL, random.choice(DialogueUtils.USER_GOALS)

	USER_GOAL, AGENT_GOAL = None, None
	@staticmethod
	def get_goal():
		DialogueUtils.USER_GOAL, DialogueUtils.AGENT_GOAL = DialogueUtils.generate_goal()

	@staticmethod
	def reward_function(done, speech_confidence, turns, status):
		reward = -1
		#episode is done
		if status == DialogueUtils.FAIL:
			#took too many turns in the current episode
			#the more turns you took the worse the penalty
			reward += -Parameters.MAX_TURNS
		elif status == DialogueUtils.SUCCESS: #successfully ended episode
			if speech_confidence == DialogueUtils.AUDIBLE_CONFIDENCE_LEVEL[0]:
				#if you have low confidence, then you want to be rewarded for higher number of turns
				reward += 2*turns
			elif speech_confidence == DialogueUtils.AUDIBLE_CONFIDENCE_LEVEL[1]:
				reward += turns
			else:
				reward += -turns
		return reward

	#[speaker, intent, object_slot, location_slot, current_slot_idx]
	
	AGENT_ACTIONS=[['agent',TERMINATE_INTENT[0], None, None, 2]]
	for intent in AGENT_INTENTS:
		for object_slot in OBJECT_SLOT_VALUES:
			for location_slot in LOCATION_SLOT_VALUES:
				if not object_slot:
					continue
				for current_slot_idx in range(len(REQUEST_SLOTS)):
					AGENT_ACTIONS.append(['agent',intent,object_slot,location_slot,current_slot_idx])

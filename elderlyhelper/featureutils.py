import itertools
from sklearn.feature_extraction import DictVectorizer
from scipy import sparse
from sklearn.preprocessing import OneHotEncoder
import numpy as np

from .dialogueutils import DialogueUtils
from .parameters import Parameters

class FeatureUtils:
	TURNS_IDX=0
	AUDIBLE_CONFIDENCE_LEVEL_IDX=1
	
	metadata=np.zeros(AUDIBLE_CONFIDENCE_LEVEL_IDX+1, dtype=object)
	metadata[TURNS_IDX]=np.asarray(range(Parameters.MAX_TURNS))
	metadata[AUDIBLE_CONFIDENCE_LEVEL_IDX]=np.asarray(DialogueUtils.AUDIBLE_CONFIDENCE_LEVEL)

	metadata_feature_extractor = OneHotEncoder(categories=metadata,sparse=False)
	metadata_feature_extractor.fit_transform([[0,'lowconf']])

	SPEAKER_IDX=0
	INTENT_IDX=1
	OBJECT_SLOT_IDX=2
	LOCATION_SLOT_IDX=3
	CURRENT_SLOT_IDX=4

	request=np.zeros(CURRENT_SLOT_IDX+1,dtype=object)

	request[SPEAKER_IDX]=np.asarray(DialogueUtils.SPEAKERS)
	request[INTENT_IDX]=np.asarray(DialogueUtils.ALL_INTENTS)
	request[OBJECT_SLOT_IDX]=\
		np.asarray(DialogueUtils.OBJECT_SLOT_VALUES+[None])
	request[LOCATION_SLOT_IDX]=\
		np.asarray(DialogueUtils.LOCATION_SLOT_VALUES+[None])
	request[CURRENT_SLOT_IDX]=\
		np.asarray(range(len(DialogueUtils.REQUEST_SLOTS)+1))

	request_feature_extractor = OneHotEncoder(categories=request,sparse=False)
	request_feature_extractor.fit_transform([['user','instr','cup','cabinet',0]])

	@staticmethod
	#Example:
	#input:
	#feature_vector = [['user','instr','cup','cabinet',0]]
	#return:
	#array([[1., 0., 0., 0., 0., 0., 0., 0., 1., 0., 0., 1., 0., 1., 0., 0.,
	#        1., 0., 1., 0., 0., 0.]])
	def transform_to_one_hot_encoding(feature_vector, feature_extractor):
		#print("FEATURE VECTOR",feature_vector)
		return feature_extractor.fit_transform([feature_vector])

	#Example:
	#input:
	#one_hot_feature_vector = 
	#array([[1., 0., 0., 0., 0., 0., 0., 0., 1., 0., 0., 1., 0., 1., 0., 0.,
	#        1., 0., 1., 0., 0., 0.]])
	#return:
	#[['user','instr','cup','cabinet',0]]
	def inverse_transform_to_feature_vector(one_hot_feature_vector, feature_extractor):
		return feature_extractor.inverse_transform([one_hot_feature_vector])
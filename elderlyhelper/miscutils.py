class MiscUtils:
	@staticmethod
	def convert_list_to_dict(lst):
		"""
		Convert list to dict where the keys are the list elements, and the values are the indices of the elements in the list.

		Parameters:
			lst (list)

		Returns:
			dict
		"""

		if len(lst) > len(set(lst)):
			raise ValueError('List must be unique!')
		return {k: v for v, k in enumerate(lst)}

	@staticmethod
	def index_dict_items(dct):
		"""
		Add index to each item in dict. For example, if dct={'a':['x','y','z']} then this method will transform dct to:
		indexed_dct={'a':{'x':0,'y':1,'z':2}}

		Parameters:
			dct (dict)

		Returns:
			dict
		"""

		idx=0
		indexed_dct={}
		for k,v in dct.items():
			indexed_dct[k]={}

			for item in v:
				indexed_dct[k][item]=idx
				idx += 1

		return indexed_dct

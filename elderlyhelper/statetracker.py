from .featureutils import FeatureUtils
from .dialogueutils import DialogueUtils
from .parameters import Parameters
import numpy as np
from scipy import sparse
import copy

class StateTracker:

	def __init__(self):
		self.max_turns = Parameters.MAX_TURNS
		self.none_state = np.zeros((self.get_state_size(),))
		self.reset()

	def get_state_size(self):
		return 2*(FeatureUtils.request_feature_extractor.get_feature_names().shape[0]) + \
			FeatureUtils.metadata_feature_extractor.get_feature_names().shape[0]

	def reset(self):
		self.history = []
		self.turns = 0

	def get_state(self, done=False):
		"""
		Returns the state representation as a numpy array which is fed into the agent's neural network.

		The state representation contains useful information for the agent about the current state of the conversation.
		Processes by the agent to be fed into the neural network. Ripe for experimentation and optimization.

		Parameters:
			done (bool): Indicates whether this is the last dialogue in the episode/conversation. Default: False

		Returns:
			numpy.array: A numpy array of shape (state size,)

		"""

		# If done then fill state with zeros
		if done:
			return self.none_state

		user_action = self.history[-1]
		last_agent_action = self.history[-2] if len(self.history) > 1 else None

		user_features = FeatureUtils.transform_to_one_hot_encoding(user_action['request'],FeatureUtils.request_feature_extractor)
		if last_agent_action:
			last_agent_features = FeatureUtils.transform_to_one_hot_encoding(last_agent_action['request'],FeatureUtils.request_feature_extractor)
		else:
			last_agent_features = np.zeros((1,FeatureUtils.request_feature_extractor.get_feature_names().shape[0]), dtype=object)
		metadata_features = FeatureUtils.transform_to_one_hot_encoding(user_action['metadata'],FeatureUtils.metadata_feature_extractor)

		state_representation=np.hstack((metadata_features,user_features,last_agent_features))

		return state_representation.reshape(-1)

	def update_state_agent(self, agent_action):
		"""
		Updates the dialogue history with the agent's action and augments the agent's action.
		"""

		agent_action['metadata'][FeatureUtils.TURNS_IDX]=self.turns
		agent_action['request'][FeatureUtils.SPEAKER_IDX]='agent'
		
		self.history.append(agent_action)

	def update_state_user(self, user_action):
		"""
		Updates the dialogue history with the user's action and augments the user's action.

		Takes a user action and updates the history. Also augments the user_action param with necessary information.

		"""
		user_action['metadata'][FeatureUtils.TURNS_IDX]=self.turns
		user_action['request'][FeatureUtils.SPEAKER_IDX]='user'

		self.history.append(user_action)
		self.turns += 1
import warnings
from warnings import simplefilter
# ignore all future warnings
simplefilter(action='ignore', category=FutureWarning)

from .dialogueutils import DialogueUtils
from .featureutils import FeatureUtils
from .parameters import Parameters
import random, copy
import numpy as np

class UserSimulator:
	def __init__(self):
		self.max_turns = Parameters.MAX_TURNS

	def reset(self):
		self.state={}
		self.state['intent']=''
		self.state['metadata']=[]
		self.state['rest'] = copy.deepcopy(DialogueUtils.USER_GOAL)
		self.state['request'] = []
		self.state['history'] = []

		return self._return_init_action()
	
	def _get_state(self):
		return self.state

	def _get_current_slot_idx(self):
		return len(DialogueUtils.REQUEST_SLOTS) - \
			len([slot for slot in self.state['rest'] if slot]) if len(self.state['rest']) < 2 \
				else len(DialogueUtils.REQUEST_SLOTS) - len(self.state['rest']) if any(self.state['rest']) else 2

	#user_response = {metadata:[None, None], request:['user', instr', 'cup', 'box', 0]}
	def _return_init_action(self):
		self.state['intent'] = 'instr'
		self.state['request']=np.zeros(FeatureUtils.CURRENT_SLOT_IDX+1, dtype=object)
		self.state['request'][FeatureUtils.SPEAKER_IDX] = 'user'
		self.state['request'][FeatureUtils.INTENT_IDX] = self.state['intent']
		self.state['request'][FeatureUtils.OBJECT_SLOT_IDX] = self.state['rest'][0]
		self.state['request'][FeatureUtils.LOCATION_SLOT_IDX] = self.state['rest'][1]

		self.state['request'][FeatureUtils.CURRENT_SLOT_IDX] = self._get_current_slot_idx()

		self.state['metadata']=np.zeros(FeatureUtils.AUDIBLE_CONFIDENCE_LEVEL_IDX+1, dtype=object)
		user_response={"metadata":self.state['metadata'],"request":self.state['request']}

		if Parameters.DEBUG:
			print("\n")
			print("Init user action: {}".format(user_response))
			print("\n")

		return user_response

	#Example input:
	#	agent_action = {'metadata':[1, 'highconf'], 'request':['agent', 'qyn', 'cup', 'box', 0]}
	def step(self, agent_action):
		self.state['intent']=''

		status=DialogueUtils.NO_OUTCOME
		done=False

		#If it took too many turns, then user is done:
		#	user_action = {metadata=[10, 'highconf'], request=['user', 'done', None, None, 2]
		if agent_action['metadata'][FeatureUtils.TURNS_IDX] >= Parameters.MAX_TURNS:
			status=DialogueUtils.FAIL
			self.state['intent'] = DialogueUtils.TERMINATE_INTENT[0]
		#Otherwise, user needs to produce appropriate response to agent intent.
		else:
			agent_intent = agent_action['request'][FeatureUtils.INTENT_IDX]

			if agent_intent in DialogueUtils.AGENT_INTENTS:
				status = self._response_to_agent_intent(agent_action)
			elif agent_intent == DialogueUtils.TERMINATE_INTENT[0]:
				status = self._response_to_done()
				self.state['intent'] = DialogueUtils.TERMINATE_INTENT[0]

		if self.state['intent'] == DialogueUtils.TERMINATE_INTENT[0]:
			done = True
			self.state['request']=np.zeros(FeatureUtils.CURRENT_SLOT_IDX+1, dtype=object)
			self.state['request'][FeatureUtils.SPEAKER_IDX] = 'user'
			self.state['request'][FeatureUtils.INTENT_IDX]=self.state['intent']
			self.state['request'][FeatureUtils.OBJECT_SLOT_IDX] = None
			self.state['request'][FeatureUtils.LOCATION_SLOT_IDX] = None
			self.state['request'][FeatureUtils.CURRENT_SLOT_IDX] = self._get_current_slot_idx()

		#user_response = {metadata=[1, highconf], request=['user', 'ry', 1, None, 'box', 'box']
		self.state['metadata']=np.zeros(FeatureUtils.AUDIBLE_CONFIDENCE_LEVEL_IDX+1, dtype=object)
		self.state['metadata'][FeatureUtils.TURNS_IDX]=agent_action['metadata'][FeatureUtils.TURNS_IDX]
		self.state['metadata'][FeatureUtils.AUDIBLE_CONFIDENCE_LEVEL_IDX]=agent_action['metadata'][FeatureUtils.AUDIBLE_CONFIDENCE_LEVEL_IDX]

		user_response={'metadata':self.state['metadata'],'request':self.state['request']}

		reward = DialogueUtils.reward_function(done, self.state['metadata'][FeatureUtils.AUDIBLE_CONFIDENCE_LEVEL_IDX],\
			self.state['metadata'][FeatureUtils.TURNS_IDX], status)

		if Parameters.DEBUG:
			print("\n")
			print("UserSim response", user_response,"Episode status",status,"Episode done",done,"Episode reward",reward)
			print("\n")

		return user_response, reward, done, True if status is 1 else False

	'''
	Agent intent is either QYN or CHK. User needs to respond.
		User should respond with RN when either of these cases are true:

		1. The OBJECT_SLOT and LOCATION_SLOT should match between user and agent. Will require checking against both self.state['rest'] and self.state['history']
		2. The self.state['rest']AGENT_CURRENT_SLOT_IDX] should match agent_action['request'][OBJECT_SLOT] or agent_action[LOCATION_SLOT] accordingly
		3. The AGENT_CURRENT_SLOT_IDX == len(self.state['rest'])

		Note that 2 is redundant, since if 1 and 3 fail matching, 2 implicitly would fail as well.

		Example:
			self.state['rest']=['box'], self.state['history']=['cup']
			agent_action=['agent','qyn','cup','box',2] 
			user_action=['user','rn',None,'box',1]

	Otherwise, user will need to respond with RY:
		Example:
			self.state['rest']=['box'], self.state['history']=['cup']
			agent_action=['agent','qyn',None,'box',1] 
			user_action=['user','ry',None,'box',1]

			self.state['rest']=[], self.state['history']=['cup','box']
	'''
	def _response_to_agent_intent(self, agent_action):
		self.state['request']=np.zeros(FeatureUtils.CURRENT_SLOT_IDX+1, dtype=object)

		#User is the speaker always when action produced by the UserSimulator
		self.state['request'][FeatureUtils.SPEAKER_IDX] = 'user'

		#Assume episode has not yet finished.
		status = DialogueUtils.NO_OUTCOME
		#if Parameters.DEBUG:
		#	print("\nUserSim Response to agent intent\n")

		agent_intent = agent_action['request'][FeatureUtils.INTENT_IDX]
		user_goal = copy.deepcopy(self.state['history']+self.state['rest'])

		agent_goal = [agent_action['request'][FeatureUtils.OBJECT_SLOT_IDX], agent_action['request'][FeatureUtils.LOCATION_SLOT_IDX]]

		self.state['request'][FeatureUtils.CURRENT_SLOT_IDX] = self._get_current_slot_idx()
		'''if Parameters.DEBUG:
			print("\n")
			print("User goal match test")
			print("\n")
			print("USER GOAL",user_goal)
			print("AGENT SLOTS",[agent_action['request'][FeatureUtils.OBJECT_SLOT_IDX], agent_action['request'][FeatureUtils.LOCATION_SLOT_IDX]])
			print("\n")

			print("\n")
			print("Current slot idx test")
			print("\n")
			print("USER current slot idx",self.state['request'][FeatureUtils.CURRENT_SLOT_IDX])
			print("\n")
			print("AGENT current slot idx",agent_action['request'][FeatureUtils.CURRENT_SLOT_IDX])
			print("\n")'''

		if user_goal != agent_goal:
			self.state['intent']='rn'
			self.state['request'][FeatureUtils.INTENT_IDX] = self.state['intent']	
			self.state['request'][FeatureUtils.CURRENT_SLOT_IDX] = self.state['request'][FeatureUtils.CURRENT_SLOT_IDX]
			self.state['request'][FeatureUtils.OBJECT_SLOT_IDX] = user_goal[0]
			self.state['request'][FeatureUtils.LOCATION_SLOT_IDX] = user_goal[1]

			if Parameters.DEBUG:
				print("\n")
				print("UserSim replying no since user goal match fails",\
					"REQ",self.state['request'],"REST",self.state['rest'],"HISTORY",self.state['history'])
				print("\n")

		elif agent_action['request'][FeatureUtils.CURRENT_SLOT_IDX] != self.state['request'][FeatureUtils.CURRENT_SLOT_IDX]:
			self.state['intent']='rn'
			self.state['request'][FeatureUtils.INTENT_IDX] = self.state['intent']
			self.state['request'][FeatureUtils.OBJECT_SLOT_IDX] = agent_action['request'][FeatureUtils.OBJECT_SLOT_IDX]
			self.state['request'][FeatureUtils.LOCATION_SLOT_IDX] = agent_action['request'][FeatureUtils.LOCATION_SLOT_IDX]

			if Parameters.DEBUG:
				print("\n")
				print("UserSim replying no since current slot idx match fails",\
					"REQ",self.state['request'],"REST",self.state['rest'],"HISTORY",self.state['history'])
				print("\n")
		elif agent_action['request'][FeatureUtils.CURRENT_SLOT_IDX] == 2:
			self.state['intent']='rn'
			self.state['request'][FeatureUtils.INTENT_IDX] = self.state['intent']
			self.state['request'][FeatureUtils.OBJECT_SLOT_IDX] = agent_action['request'][FeatureUtils.OBJECT_SLOT_IDX]
			self.state['request'][FeatureUtils.LOCATION_SLOT_IDX] = agent_action['request'][FeatureUtils.LOCATION_SLOT_IDX]

			if Parameters.DEBUG:
				print("\n")
				print("UserSim replying no since current slot idx match fails",\
					"REQ",self.state['request'],"REST",self.state['rest'],"HISTORY",self.state['history'])
				print("\n")
		else:
			self.state['intent']='ry'
			self.state['request'][FeatureUtils.INTENT_IDX] = self.state['intent']
			self.state['request'][FeatureUtils.OBJECT_SLOT_IDX] = agent_action['request'][FeatureUtils.OBJECT_SLOT_IDX]
			self.state['request'][FeatureUtils.LOCATION_SLOT_IDX] = agent_action['request'][FeatureUtils.LOCATION_SLOT_IDX]

			slot=agent_action['request'][ 2+agent_action['request'][FeatureUtils.CURRENT_SLOT_IDX] ]
			self.state['rest'].remove(slot)
			self.state['history'].append(slot)

			if Parameters.DEBUG:
				print("\n")
				print("UserSim replying yes since match succeeds",\
					"REQ",self.state['request'],"REST",self.state['rest'],"HISTORY",self.state['history'])
				print("\n")

		return status

	def _response_to_done(self):
		if self.state['rest']:
			return DialogueUtils.FAIL

		return DialogueUtils.SUCCESS

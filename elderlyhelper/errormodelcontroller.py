import random
from .dialogueutils import DialogueUtils
from .featureutils import FeatureUtils

class ErrorModelController:
	"""Adds error to the user action."""

	def __init__(self):
		"""
		The constructor for ErrorModelController.

		We need to model a stochastic audible environment. Even if the user vocally asks for "cup", the 
		automatic speech recognition confidence (speech_confidence) and slot similarity values can be low, indicating
		that it may not be reliable enough audible from the environment.

		Since the state tracker (the subsequent component in the pipeline) acts as the "note taking" by the agent of what it 
		thinks about the environment (recall: this is a model free approach).
		"""
		self.speech_confidence = float(0)
		self.slot_similarity = float(0)

	def reset(self):
		'''
		The goal here is to simulate the speech_confidence and slot_similarity values but only the first time. In other words,
		the entire conversation is dictated by what the agent hears the first time the user speaks. Hence why we perform the random
		the random generation in the constructor only.
		'''

		self.speech_confidence = random.random()
		self.slot_similarity = random.random()

	def infuse_error(self, user_action):
		"""
		Takes a user action as a dict and adds 'error' to it. First, the user_action is expanded: inform_slots is appended with
		speech_confidence and slot_similarity

		Parameters:
			example user_action (dict): {'intent': <user intent>, 'inform_slots': {}, 'request_slots': {'object':'ball'})
		"""


		audible_error_prob = 1 - (2/((1/self.speech_confidence) + (1/self.slot_similarity)))

		if audible_error_prob < 0.33:
			audible_conf = DialogueUtils.AUDIBLE_CONFIDENCE_LEVEL[0]
		elif audible_error_prob < 0.67:
			audible_conf = DialogueUtils.AUDIBLE_CONFIDENCE_LEVEL[1]
		else:
			audible_conf = DialogueUtils.AUDIBLE_CONFIDENCE_LEVEL[2]

		user_action['metadata'][FeatureUtils.AUDIBLE_CONFIDENCE_LEVEL_IDX] = audible_conf

		if random.random() > audible_error_prob:
			if user_action['request'][FeatureUtils.OBJECT_SLOT_IDX]:
				user_action['request'][FeatureUtils.OBJECT_SLOT_IDX] = \
					random.choice(DialogueUtils.OBJECT_SLOT_VALUES)

			if user_action['request'][FeatureUtils.LOCATION_SLOT_IDX]:
				user_action['request'][FeatureUtils.LOCATION_SLOT_IDX] = \
					random.choice(DialogueUtils.LOCATION_SLOT_VALUES)

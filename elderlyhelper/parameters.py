class Parameters:
	DEBUG=False

	USERSIM=True
	AUD_CONF_THRESHOLD=0.70

	WARMUP_MEM=1_000
	MAX_EPISODES=6_000

	TRAIN_FREQ=100
	MAX_TURNS=20
	SUCCESS_RATE_THRESHOLD=0.3

	SAVE_WEIGHTS_FILE_PATH='saved_models/model.h5'

	VANILLA=True
	LEARNING_RATE=1e-3
	BATCH_SIZE=64
	DQN_HIDDEN_SIZE=80
	GAMMA=0.99
	MAX_MEM_SIZE=500000

	EPSILON_INIT = 0.0
	START_EPSILON_DECAYING = 1
	END_EPSILON_DECAYING = MAX_EPISODES//2
	EPSILON_DECAY_VALUE = EPSILON_INIT/(END_EPSILON_DECAYING - START_EPSILON_DECAYING)

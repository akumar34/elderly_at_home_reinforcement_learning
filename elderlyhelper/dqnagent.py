import tensorflow
from keras.models import Sequential
from keras.layers import Dense
from keras.optimizers import Adam
import random, copy
import numpy as np
from collections import OrderedDict
from .dialogueutils import DialogueUtils
from .parameters import Parameters
from .featureutils import FeatureUtils
import re

class DQNAgent:
	"""The DQN agent that interacts with the user."""
	state_space_freq=OrderedDict()
	stat_freq=OrderedDict()
	def __init__(self, state_size):
		"""
		The constructor of DQNAgent.

		The constructor of DQNAgent which saves constants, sets up neural network graphs, etc.

		Parameters:
			state_size (int): The state representation size or length of numpy array
			constants (dict): Loaded constants in dict

		"""
		self.memory = []
		self.memory_index = 0
		self.max_memory_size = Parameters.MAX_MEM_SIZE
		#self.eps = Parameters.EPSILON_INIT
		self.aud_conf_threshold = Parameters.AUD_CONF_THRESHOLD
		self.vanilla = Parameters.VANILLA
		self.lr = Parameters.LEARNING_RATE
		self.gamma = Parameters.GAMMA
		self.batch_size = Parameters.BATCH_SIZE
		self.hidden_size = Parameters.DQN_HIDDEN_SIZE

		self.load_weights_file_path = Parameters.LOAD_WEIGHTS_FILE_PATH
		self.save_weights_file_path = Parameters.SAVE_WEIGHTS_FILE_PATH

		if self.max_memory_size < self.batch_size:
			raise ValueError('Max memory size must be at least as great as batch size!')

		self.state_size = state_size
		self.possible_actions = DialogueUtils.AGENT_ACTIONS
		self.num_actions = len(self.possible_actions)

		self.rule_request_set = copy.deepcopy(DialogueUtils.AGENT_GOAL)

		self.beh_model = self._build_model()
		self.tar_model = self._build_model()

		self._load_weights()

		self.reset()
	
	@staticmethod
	def get_state_space_freq():
		return DQNAgent.state_space_freq

	@staticmethod
	def get_stat_freq():
		return DQNAgent.stat_freq
	
	def _build_model(self):
		"""Builds and returns model/graph of neural network."""

		model = Sequential()
		model.add(Dense(self.hidden_size, input_dim=self.state_size, activation='relu'))
		model.add(Dense(self.num_actions, activation='linear'))
		model.compile(loss='mse', optimizer=Adam(lr=self.lr))
		return model

	def reset(self):
		"""Resets the rule-based variables."""
		#self.rule_request_set = copy.deepcopy(DialogueUtils.agent_goal)
		self.rule_request_set = copy.deepcopy(DialogueUtils.AGENT_GOAL)
		self.rule_phase = 'not done'
		self.rule_current_slot_index = 0

	def get_action(self, user_action, state, use_rule):
		"""
		Returns the action of the agent given a state.

		Gets the action of the agent given the current state. Either the rule-based policy or the neural networks are
		used to respond.

		Parameters:
			state (numpy.array): The database with format dict(long: dict)
			use_rule (bool): Indicates whether or not to use the rule-based policy, which depends on if this was called
							 in warmup or training. Default: False

		Returns:
			int: The index of the action in the possible actions
			dict: The action/response itself

		"""
		if Parameters.DEBUG:
			print('\n')
			if use_rule:
				print("DQNAgent - RULEBASED: receives user action",user_action)
			else:
				print("DQNAgent - NN: receives user action",user_action)
			print('\n')

		action = None
		index = -1
		if random.random() < Parameters.EPSILON_INIT:
			index = random.randint(0, self.num_actions - 1)
			action = self._map_index_to_action(index)
		else:
			if use_rule:
				index, action = self._rule_action()
			else:
				index, action = self._dqn_action(state)

		'''if Parameters.DEBUG:
			print("\n")
			if use_rule:
				print("DQNAGENT - RULEBASED: MAIN: UA INFORMS",user_action['metadata'])
			else:
				print("DQNAGENT - NN: MAIN: UA INFORMS",user_action['metadata'])
			print("\n")'''

		agent_response={'metadata':np.zeros(FeatureUtils.AUDIBLE_CONFIDENCE_LEVEL_IDX+1, dtype=object),'request':action}
		agent_response['metadata'][FeatureUtils.TURNS_IDX] = user_action['metadata'][FeatureUtils.TURNS_IDX]
		agent_response['metadata'][FeatureUtils.AUDIBLE_CONFIDENCE_LEVEL_IDX] = user_action['metadata'][FeatureUtils.AUDIBLE_CONFIDENCE_LEVEL_IDX]
		if Parameters.DEBUG:
			print('\n')
			if use_rule:
				print("DQNAGENT - RULEBASED: MAIN: Responds to user with agent action", agent_response, "and index", index)
			else:
				print("DQNAGENT - NN: MAIN: Responds to user with agent action", agent_response, "and index", index)
			print('\n')
		return index, agent_response

	def _rule_action(self):
		"""
		Returns a rule-based policy action.

		Selects the next action of a simple rule-based policy.

		Returns:
		int: The index of the action in the possible actions
			dict: The action/response itself

		"""

		if Parameters.DEBUG:
			print('\n')
			print("DQNAgent - RULEBASED: rules set",self.rule_request_set)
			print('\n')
			print("DQNAgent - RULEBASED: current index",self.rule_current_slot_index,"and length",len(self.rule_request_set))
			print('\n')

		action = None
		#print("DQNAGENT - RULEBASED: current idx",self.rule_current_slot_index,"and rule request set length",len(self.rule_request_set))	
		if self.rule_current_slot_index < len(self.rule_request_set):
			if Parameters.DEBUG:
				print("\n")
				print("DQNAgent - RULEBASED: current slot not finished")
				print("\n")

			action = np.zeros(FeatureUtils.CURRENT_SLOT_IDX+1,dtype=object)
			action[FeatureUtils.SPEAKER_IDX] = 'agent'
			action[FeatureUtils.INTENT_IDX] = random.choice(['qyn','chk'])
			action[FeatureUtils.OBJECT_SLOT_IDX] = self.rule_request_set[0]
			action[FeatureUtils.LOCATION_SLOT_IDX] = self.rule_request_set[1]
			action[FeatureUtils.CURRENT_SLOT_IDX] = self.rule_current_slot_index

			self.rule_current_slot_index += 1
		else:
			if Parameters.DEBUG:
				print("\n")
				print("DQNAgent - RULEBASED: does not enter if statement")
				print("\n")
			action = np.zeros(FeatureUtils.CURRENT_SLOT_IDX+1,dtype=object)
			action[FeatureUtils.SPEAKER_IDX] = 'agent'
			action[FeatureUtils.INTENT_IDX] = 'done'
			action[FeatureUtils.OBJECT_SLOT_IDX] = None
			action[FeatureUtils.LOCATION_SLOT_IDX] = None
			action[FeatureUtils.CURRENT_SLOT_IDX] = 2

		action=list(action)
		if Parameters.DEBUG:
			print('\n')
			print("DQNAGENT - RULEBASED: Responds to user with agent action", action)
			print('\n')
		index = self._map_action_to_index(action)
		return index, action

	def _map_action_to_index(self, response):
		"""
		Maps an action to an index from possible actions.

		Parameters:
			response (dict)

		Returns:
			int
		"""
		'''for (i, action) in enumerate(self.possible_actions):
			if Parameters.DEBUG:
				print("\n")
				print(i, self.possible_actions[i])
				print("\n")'''
		#print("POSSIBLE ACTIONS",self.possible_actions,"RESPONSE",response)
		for (i, action) in enumerate(self.possible_actions):
			if response == action:

				if i not in DQNAgent.state_space_freq:
					DQNAgent.state_space_freq[i]=0
				DQNAgent.state_space_freq[i]+=1

				return i
		raise ValueError('Response: {} not found in possible actions'.format(response))

	def _dqn_action(self, state):
		"""
		Returns a behavior model output given a state.

		Parameters:
			state (numpy.array)

		Returns:
			int: The index of the action in the possible actions
			dict: The action/response itself
		"""
		predictions = self._dqn_predict_one(state)
		index = np.argmax(predictions)

		if Parameters.DEBUG:
			metadata_size=FeatureUtils.metadata_feature_extractor.get_feature_names().shape[0]
			request_size=FeatureUtils.request_feature_extractor.get_feature_names().shape[0]

			'''print('\n')
			print("DQN AGENT - INFORM",FeatureUtils.inverse_transform_to_feature_vector(state[:metadata_size],FeatureUtils.metadata_feature_extractor))
			print("DQN AGENT - AGENT",FeatureUtils.inverse_transform_to_feature_vector(state[metadata_size+request_size:],FeatureUtils.request_feature_extractor))
			print("DQN AGENT - USER",FeatureUtils.inverse_transform_to_feature_vector(state[metadata_size:metadata_size+request_size],FeatureUtils.request_feature_extractor))

			for i,action in enumerate(DialogueUtils.AGENT_ACTIONS):
				print("DQN AGENT:",i,action,predictions[i])
			print("DQN AGENT: best qvalue",index,DialogueUtils.AGENT_ACTIONS[index])'''

		action = self._map_index_to_action(index)
		return index, action

	def _map_index_to_action(self, index):
		"""
		Maps an index to an action in possible actions.

		Parameters:
			index (int)

		Returns:
			dict
		"""

		'''for (i, action) in enumerate(self.possible_actions):
			if Parameters.DEBUG:
				print("\n")
				print(i, self.possible_actions[i])
				print("\n")'''

		for (i, action) in enumerate(self.possible_actions):
			if index == i:
				return copy.deepcopy(action)
		#if index < len(self.possible_actions):
		#	return copy.deepcopy(self.possible_actions[index])
		raise ValueError('Index: {} not in range of possible actions'.format(index))

	def _dqn_predict_one(self, state, target=False):
		"""
		Returns a model prediction given a state.

		Parameters:
			state (numpy.array)
			target (bool)

		Returns:
			numpy.array
		"""

		return self._dqn_predict(state.reshape(1, self.state_size), target=target).flatten()

	def _dqn_predict(self, states, target=False):
		"""
		Returns a model prediction given an array of states.

		Parameters:
			states (numpy.array)
			target (bool)

		Returns:
			numpy.array
		"""

		if target:
			return self.tar_model.predict(states)
		else:
			return self.beh_model.predict(states)

	#@staticmethod
	#def get_state_space_freq():
	#	return DQNAgent.state_space_freq

	def add_experience(self, state, action, reward, next_state, done):
		"""
		Adds an experience tuple made of the parameters to the memory.

		Parameters:
			state (numpy.array)
			action (int)
			reward (int)
			next_state (numpy.array)
			done (bool)

		"""

		if Parameters.DEBUG:
			metadata_size=FeatureUtils.metadata_feature_extractor.get_feature_names().shape[0]
			request_size=FeatureUtils.request_feature_extractor.get_feature_names().shape[0]

			print('\n')
			print("DQN AGENT - INFORM",FeatureUtils.inverse_transform_to_feature_vector(state[:metadata_size],FeatureUtils.metadata_feature_extractor))
			print("DQN AGENT - AGENT",FeatureUtils.inverse_transform_to_feature_vector(state[metadata_size+request_size:],FeatureUtils.request_feature_extractor))
			print("DQN AGENT - USER",FeatureUtils.inverse_transform_to_feature_vector(state[metadata_size:metadata_size+request_size],FeatureUtils.request_feature_extractor))

			print("DQN AGENT - NEXT INFORM",FeatureUtils.inverse_transform_to_feature_vector(next_state[:metadata_size],FeatureUtils.metadata_feature_extractor))
			print("DQN AGENT - NEXT AGENT",FeatureUtils.inverse_transform_to_feature_vector(next_state[metadata_size+request_size:],FeatureUtils.request_feature_extractor))
			print("DQN AGENT - NEXT USER",FeatureUtils.inverse_transform_to_feature_vector(next_state[metadata_size:metadata_size+request_size],FeatureUtils.request_feature_extractor))
			print('\n')

		feature_tuple = tuple(list(state))
		if feature_tuple not in DQNAgent.stat_freq:
			DQNAgent.stat_freq[feature_tuple]=0
		DQNAgent.stat_freq[feature_tuple]+=1

		if len(self.memory) < self.max_memory_size:
			self.memory.append(None)
		self.memory[self.memory_index] = state, \
			action, reward, next_state, done
		self.memory_index = (self.memory_index + 1) % self.max_memory_size

	def empty_memory(self):
		"""Empties the memory and resets the memory index."""

		self.memory = []
		self.memory_index = 0

	def is_memory_full(self):
		"""Returns true if the memory is full."""

		return len(self.memory) == self.max_memory_size

	def train(self):
		"""
		Trains the agent by improving the behavior model given the memory tuples.

		Takes batches of memories from the memory pool and processing them. The processing takes the tuples and stacks
		them in the correct format for the neural network and calculates the Bellman equation for Q-Learning.

		"""

		# Calc. num of batches to run
		num_batches = len(self.memory) // self.batch_size
		for b in range(num_batches):
			batch = random.sample(self.memory, self.batch_size)

			states = np.array([sample[0] for sample in batch])
			next_states = np.array([sample[3] for sample in batch])

			assert states.shape == (self.batch_size, self.state_size), 'States Shape: {}'.format(states.shape)
			assert next_states.shape == states.shape

			beh_state_preds = self._dqn_predict(states)  # For leveling error
			if not self.vanilla:
				beh_next_states_preds = self._dqn_predict(next_states)  # For indexing for DDQN
			tar_next_state_preds = self._dqn_predict(next_states, target=True)  # For target value for DQN (& DDQN)

			inputs = np.zeros((self.batch_size, self.state_size))
			targets = np.zeros((self.batch_size, self.num_actions))

			for i, (s, a, r, s_, d) in enumerate(batch):
				t = beh_state_preds[i]
				if not self.vanilla:
					t[a] = r + self.gamma * tar_next_state_preds[i][np.argmax(beh_next_states_preds[i])] * (not d)
				else:
					t[a] = r + self.gamma * np.amax(tar_next_state_preds[i]) * (not d)

				inputs[i] = s
				targets[i] = t

			self.beh_model.fit(inputs, targets, epochs=1, verbose=0)

	def copy(self):
		"""Copies the behavior model's weights into the target model's weights."""

		self.tar_model.set_weights(self.beh_model.get_weights())

	def save_weights(self):
		"""Saves the weights of both models in two h5 files."""

		if not self.save_weights_file_path:
			return
		beh_save_file_path = re.sub(r'\.h5', r'_beh.h5', self.save_weights_file_path)
		print("Saving beh model to",beh_save_file_path)
		self.beh_model.save_weights(beh_save_file_path)

		tar_save_file_path = re.sub(r'\.h5', r'_tar.h5', self.save_weights_file_path)
		print("Saving tar model to",tar_save_file_path)
		self.tar_model.save_weights(tar_save_file_path)

	def _load_weights(self):
		"""Loads the weights of both models from two h5 files."""

		if not self.load_weights_file_path:
			return
		beh_load_file_path = re.sub(r'\.h5', r'_beh.h5', self.load_weights_file_path)
		self.beh_model.load_weights(beh_load_file_path)
		tar_load_file_path = re.sub(r'\.h5', r'_tar.h5', self.load_weights_file_path)
		self.tar_model.load_weights(tar_load_file_path)

import elderlyhelper
import pickle, argparse, json, math, copy, os
import numpy as np
os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

if __name__ == "__main__":
	# 1) In terminal: python test.py
	# 2) Run this file as is

	USE_USERSIM = elderlyhelper.Parameters.USERSIM
	MAX_TURNS = elderlyhelper.Parameters.MAX_TURNS
	elderlyhelper.Parameters.EPSILON_INIT = 0.0
	elderlyhelper.Parameters.LOAD_WEIGHTS_FILE_PATH='saved_models/model.h5'
	elderlyhelper.Parameters.MAX_EPISODES = 3_000

	if USE_USERSIM:
		user = elderlyhelper.UserSimulator()
	else:
		user = elderlyhelper.User()

	emc = elderlyhelper.ErrorModelController()

	state_tracker = elderlyhelper.StateTracker()

	dqn_agent = elderlyhelper.DQNAgent(state_tracker.get_state_size())

def test_run():
	"""
	Runs the loop that evaluates the agent.

	Setting USERSIM=True in parameters.py enables the user simulator to participate in the conversation and USERSIM=False enables
	a console prompt on the terminal whenever it is the user's turn to respond. Example console prompt responses:
		#  > Response: //speaker:user, intent:instr, object:cup, location:cabinet, idx:0
		#  > Response: //speaker:user, intent:ry, object:cup, location:cabinet, idx:0
		#  > Response: //speaker:user, intent:ry, object:cup, location:cabinet, idx:1
		#  > Response: //speaker:user, intent:done, object:None, location:None, idx:2
	"""

	print('Testing Started...')
	episode = 0
	avg_success=0
	avg_turns=0
	while episode < elderlyhelper.Parameters.MAX_EPISODES: #Iterate through all the episodes
		user_action = episode_reset()
		episode += 1
		ep_reward = 0
		done = False
		state = state_tracker.get_state()
		turns=0
		while not done: #loop through episode. One episode is complete when user goal is completed or too many turns have happened
			next_user_action, next_state, reward, done, success = run_round(user_action, state, warmup=False)
			ep_reward += reward
			user_action = next_user_action
			state=next_state
			turns+=1
			avg_turns+=1
		print('Episode: {} Done: {} Success: {} Reward: {} Turns: {}'.format(episode,done,success,ep_reward,turns))

		avg_success += success
	print('...Testing Ended')

	print("Total success: {} out of total episodes: {} results in average: {} and average turns is {}\n".\
		format(avg_success,episode,float(avg_success)/float(episode),float(avg_turns)/float(episode)))

def episode_reset():
	"""
	Resets the episode/conversation in the warmup and training loops.
	Called in warmup and train to reset the state tracker, user and agent. Also get's the initial user action.
	"""
	# Generate a random user goal
	elderlyhelper.DialogueUtils.get_goal()
	# First reset the state tracker
	state_tracker.reset()
	# Then pick an init user action
	user_action = user.reset()
	# Infuse with error (probabilistically alter slot names and also generate new ASR confidence values and slot similarity values)
	emc.reset()
	emc.infuse_error(user_action)
	# And update state tracker
	state_tracker.update_state_user(user_action)
	# Finally, reset agent
	dqn_agent.reset()

	return user_action

def run_round(user_action, state, warmup):
	# 1) Agent takes action given state tracker's representation of dialogue (state)
	agent_action_index, agent_action = dqn_agent.get_action(user_action, state, use_rule=warmup)
	# 2) Update state tracker with the agent's action
	state_tracker.update_state_agent(agent_action)
	# 3) User takes action given agent action
	user_action, reward, done, success = user.step(agent_action)
	# 4) Update state tracker with user action
	state_tracker.update_state_user(user_action)
	# 5) Get next state and add experience
	next_state = state_tracker.get_state(done)

	return user_action, next_state, reward, done, success

test_run()

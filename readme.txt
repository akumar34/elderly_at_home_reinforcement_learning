How to run:

> python train.py 

Example console output:
/home/beanz/anaconda3/envs/python3.6/lib/python3.6/site-packages/h5py/__init__.py:36: FutureWarning: Conversion of the second argument of issubdtype from `float` to `np.floating` is deprecated. In future, it will be treated as `np.float64 == np.dtype(float).type`.
  from ._conv import register_converters as _register_converters
Using TensorFlow backend.
Warmup Started...
...Warmup Ended
...Warmup Ended
Training Started...
Episode: 500 NEW BEST SUCCESS RATE: 0.14 NEW BEST Avg Reward: -34.4
Saving beh model to saved_models/model_beh.h5
Saving tar model to saved_models/model_tar.h5
Episode: 600 NEW BEST SUCCESS RATE: 0.27 NEW BEST Avg Reward: -29.68
Saving beh model to saved_models/model_beh.h5
Saving tar model to saved_models/model_tar.h5
Episode: 700 NEW BEST SUCCESS RATE: 0.56 NEW BEST Avg Reward: -22.04
Saving beh model to saved_models/model_beh.h5
Saving tar model to saved_models/model_tar.h5
Training episode 1000 of 6000 and turns 18.053

Training episode 2000 of 6000 and turns 13.765

Episode: 2100 NEW BEST SUCCESS RATE: 0.68 NEW BEST Avg Reward: -16.03
Saving beh model to saved_models/model_beh.h5
Saving tar model to saved_models/model_tar.h5
Episode: 2200 NEW BEST SUCCESS RATE: 0.76 NEW BEST Avg Reward: -14.41
Saving beh model to saved_models/model_beh.h5
Saving tar model to saved_models/model_tar.h5
Episode: 2300 NEW BEST SUCCESS RATE: 0.78 NEW BEST Avg Reward: -14.13
Saving beh model to saved_models/model_beh.h5
Saving tar model to saved_models/model_tar.h5
Training episode 3000 of 6000 and turns 11.92

Episode: 3000 NEW BEST SUCCESS RATE: 0.79 NEW BEST Avg Reward: -11.36
Saving beh model to saved_models/model_beh.h5
Saving tar model to saved_models/model_tar.h5
Episode: 3100 NEW BEST SUCCESS RATE: 0.8 NEW BEST Avg Reward: -11.4
Saving beh model to saved_models/model_beh.h5
Saving tar model to saved_models/model_tar.h5
Episode: 3500 NEW BEST SUCCESS RATE: 0.87 NEW BEST Avg Reward: -10.42
Saving beh model to saved_models/model_beh.h5
Saving tar model to saved_models/model_tar.h5
Training episode 4000 of 6000 and turns 9.862

Episode: 4000 NEW BEST SUCCESS RATE: 0.92 NEW BEST Avg Reward: -7.35
Saving beh model to saved_models/model_beh.h5
Saving tar model to saved_models/model_tar.h5
Episode: 4200 NEW BEST SUCCESS RATE: 0.93 NEW BEST Avg Reward: -7.12
Saving beh model to saved_models/model_beh.h5
Saving tar model to saved_models/model_tar.h5
Episode: 4300 NEW BEST SUCCESS RATE: 0.95 NEW BEST Avg Reward: -7.15
Saving beh model to saved_models/model_beh.h5
Saving tar model to saved_models/model_tar.h5
Episode: 4700 NEW BEST SUCCESS RATE: 1.0 NEW BEST Avg Reward: -6.01
Saving beh model to saved_models/model_beh.h5
Saving tar model to saved_models/model_tar.h5

Background notes on implementation:
Markov Decision Process(Actions,States,Rewards,Transitions,Discount)
- Discount = 0.99
- Reward:
	Return the reward given how we faired on the episode. If the episode is taking too long (e.g., too many turns),
	then penalize. On the other hand if the episode finished successfully, then the less confident the agent was
	in hearing the user, then it should be rewarded for taking more turns. If the confidence is high, then the agent
	should be rewarded for taking less turns. 

	If the episode has not finished yet, then just penalize a score of -1.

	Parameters:
	    done indicates if episode is done.
	    speech_confidence indicates how confident agent was in hearing the user correctly.
	    turns: number of turns within the current episode.
	    status: has the current episode succeeded, failed (e.g., exceeded maximum turns), or simply not finished yet.

	Returns:
	    int: Reward

	def reward_function(done, speech_confidence, turns, status):
		reward = -1
		#episode is done
		if status == DialogueUtils.FAIL:
			#took too many turns in the current episode
			#the more turns you took the worse the penalty
			reward += -Parameters.MAX_TURNS
		elif status == DialogueUtils.SUCCESS: #successfully ended episode
			if speech_confidence == DialogueUtils.AUDIBLE_CONFIDENCE_LEVEL[0]:
				#if you have low confidence, then you want to be rewarded for higher number of turns
				reward += 2*turns
			elif speech_confidence == DialogueUtils.AUDIBLE_CONFIDENCE_LEVEL[1]:
				reward += turns
			else:
				reward += -turns
		return reward

- Action:
	An example initial user_action is described below:
	{'metadata':[0, 0], 'request':['user', 'instr', 'cup', 'box', 0]}
	
	Agent follows same format, for example:
		Example:
		{'metadata':[1, 'highconf'], 'request':['agent', 'qyn', 'cup', 'box', 0]}

	User action:
		Metadata information comprises of:
			TURN = Current number of turns in conversation
			AUDIBLE_CONFIDENCE_LEVEL = 'lowconf', 'medconf', or 'highconf'
				Note: This should account for two factors:
							1. Speaker confidence = value between 0 and 1, where 1 is very high confidence in what the system hears the user say.
			
							2. Slot similarity = value between 0 and 1, where 1 is very high confidence that the user said a slot that is in the agent dictionary. If the user says "Get me a cop", then slot_similarity("cup","cop") is high indicating that "cop" is close to "cup". Note this is important: we desire the agent learn that even when speech_confidence is high, if slot_similarity is low, then it might be better to proceed with verification rather than the agent transmitting an ack early.
		Request information comprises of:
			SPEAKER = user is the speaker
			INTENT = ry, rn, rw, instr, done
			OBJECT_SLOT = cup, ball
			LOCATION_SLOT = drawer, cabinet, box
			CURRENT_SLOT_IDX=0, 1, or 2

		Note: Object can be:
			Either, object and location are BOTH None (when the intent is done), or you must specify BOTH.

	Agent action (Same as user action)
		Metadata information comprises of:
			TURN = Current number of turns in conversation
			AUDIBLE_CONFIDENCE_LEVEL = 'lowconf', 'medconf', or 'highconf'
				Note: This should account for two factors:
							1. Speaker confidence = value between 0 and 1, where 1 is very high confidence in what the system hears the user say.
			
							2. Slot similarity = value between 0 and 1, where 1 is very high confidence that the user said a slot that is in the agent dictionary. If the user says "Get me a cop", then slot_similarity("cup","cop") is high indicating that "cop" is close to "cup". Note this is important: we desire the agent learn that even when speech_confidence is high, if slot_similarity is low, then it might be better to proceed with verification rather than the agent transmitting an ack early.
		Request information comprises of:
			SPEAKER = agent is the speaker
			INTENT = qyn, chk, done
			OBJECT_SLOT = cup, ball
			LOCATION_SLOT = drawer, cabinet, box
			CURRENT_SLOT_IDX=0, 1, or 2

		Note: Object can be:
			Either, object and location are BOTH None (when the intent is done), or you must specify BOTH of them.

	Note: all the template possible actions that can be taken by the agent are in DialogueUtils.AGENT_ACTIONS. Hence, the neural network can select which of the list of possible actions is the best (e.g., select greedily based on largest Q-value)

- State:
	The state is a one-hot encoding of various state variables:
		1. Metadata information.
		2. Last agent request.
		3. The responding user request.

Class UserSimulator:
	Goal: Simulates user behavior producing a user action. Also, identifies a user goal and then evaluates the agent (in terms of reward) based on how the agent acts in successful progress towards satisfying the goal of the user.

	User goals:
		Each episode is a single conversation. Each conversation is oriented towards a specific user goal.
		The DialogueUtils.user_goals contains all possible goals. The start of each episode randomly selects
		one of these user goals.

		- Example user goal:
			A. Get me a cup from the drawer
			B. User goal for this utterance is the request slots (cup, drawer).
		- Example conversation for this user goal (cup, drawer) is:
			- First, the user focuses on cup.
			Agent: Did you say ball? 
			User: No, cup.
			Agent: Ok so cup?
			User: Yes, cup.
			Agent: Ok.

			- Then, the user focuses on drawer.
			Agent: And you want it from the drawer?
			User: Yes.
			Agent: Ok.

	Reset method:
		Returns an initial user action based on the user goal. The rest_slots internal state tracks the remaining slots to request still.

	Step method:
		Takes as input the agent_action. It returns whether the episode is done, the reward, and the response from the user (e.g., user action). Finally the status flag indicating whether an episode completed
		successfully (e.g., SUCCESS or FAIL) or episode is not done yet (NO_OUTCOME).

		Thoughts\notes in implementing this:
		Need to determine whether we have reached the end of an episode and map this to reward.
			The end of the current episode occurs when either the max number of turns has exceeded threshold or the user goal is satisfied (i.e., agent verifies the object and location slots that the user initiated.)

	Reward method:
		See description from earlier.

State tracking
	The purpose is to maintain history of each turn (e.g., system action (Did you say ball?) + user action (Yes ball))
		history[-1] gives latest user action and history[-2] gives latest agent action. E.g., (history[-1],history[-2]) gives the latest turn).

	The other purpose is to transform a given user action or agent action into a feature vector representation that can be used as input to the Deep Q neural net

	reset method
		clears the history, turns (e.g., total turns so far), and last_known_agent_request_slot (e.g., in case user request slot is empty, we can guess that the last known slot that the user mentioned is the same one being talked about this time)

	get_state method
		Transforms the metadata  respective agent and user into a one hot encoding representation.
		Essentially this is used as the input to our Deep Q network.

		Metadata (23 bits) + Last agent request (19 bits) + Last user request (19 bits) = 61 dimensional feature vector

Error Infusion:
	For the user action coming as input to the agent, the slots object (or location) can be altered stochastically. This is done based on the harmonic mean of the speech_confidence and slot_similarity values. When the agent is highly confident in what it heard from the user (e.g., high harmonic mean), then likely the request slot would not be altered. When harmonic mean is lower, then it is more likely that the request slot is altered.

Agent
	- Thoughts\notes on modeling agent:
	Why would agent transmit a 'done' intent? If the agent is confident in what it heard from the user, it does not need to verify at all (or just a basic verification). In this case Agent may send an early 'done'. Otherwise it is less confident and has a deeper conversation with the user until it is confident in completing the verification process.

	The question is how do we model the agent's confidence? We do this by accounting for (1) Speech confidence and (2) Does the request slot it heard from the user match close enough to its own dictionary of slots? Example: The agent obtains a speech confidence of 0.85 and it heard the user
	say object="top" even though the user said "cup". As the agent has dictionary of slots {"object"=["cup","ball"], "location"=["box","cabinet",'drawer']} then since slot_sim = sim("top","cup") = 0.55 then agent is less confident compared to if it had heard "cop" or of course "cup". Then (speech confidence = 0.85,  slot_sim = 0.55) is rewarded against number of turns and the success flag. The policy that the agent learns will balance sending a 'done' early VS being less confident and having deeper conversation before sending the 'done' intent.

	build_model method:
		Builds a neural network with a single hidden layer and RELU and Linear activations for appropriate layers.

		Note that this neural network leverages replay memory strategy to build a batch of training data.

	rule_action method:
		For the given user_action, respond using rule based policy. This is part of warm up, which helps the
		neural network "warm up" and train better. The purpose is that of Exploration NOT exploitation. This means, that 
		the agent needs to explore enough of the state-action space. This should be better than an epsilon-greedy agent behavior.

	dqn_action method:
		The key idea is that the neural network predicts Q values, one for each possible agent action (specified in DialogueUtils.agent_actions). Then just select the agent action corresponding to the highest Q value.
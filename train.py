import elderlyhelper
import pickle, argparse, json, math, copy
import os
os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

if __name__ == "__main__":
	# 1) In terminal: python train.py
	# 2) Run this file as is
	path="saved_models"
	for file in os.scandir(path):
		if file.name.endswith(".h5"):
			os.unlink(file.path)

	elderlyhelper.Parameters.LOAD_WEIGHTS_FILE_PATH=''

	# Load run constants
	WARMUP_MEM = elderlyhelper.Parameters.WARMUP_MEM
	MAX_EPISODES = elderlyhelper.Parameters.MAX_EPISODES
	TRAIN_FREQ = elderlyhelper.Parameters.TRAIN_FREQ
	MAX_TURNS = elderlyhelper.Parameters.MAX_TURNS
	SUCCESS_RATE_THRESHOLD = elderlyhelper.Parameters.SUCCESS_RATE_THRESHOLD

	user = elderlyhelper.UserSimulator()

	emc = elderlyhelper.ErrorModelController()

	state_tracker = elderlyhelper.StateTracker()

	dqn_agent = elderlyhelper.DQNAgent(state_tracker.get_state_size())

def run_round(user_action, state, warmup):
	# 1) Agent takes action given state tracker's representation of dialogue (state)
	agent_action_index, agent_action = dqn_agent.get_action(user_action, state, use_rule=warmup)
	# 2) Update state tracker with the agent's action
	state_tracker.update_state_agent(agent_action)
	# 3) User takes action given agent action
	user_action, reward, done, success = user.step(agent_action)
	# 4) Update state tracker with user action
	state_tracker.update_state_user(user_action)
	# 5) Get next state and add experience
	next_state = state_tracker.get_state(done)
	dqn_agent.add_experience(state, agent_action_index, reward, next_state, done)

	return user_action, next_state, reward, done, success

def warmup_run():
	"""
	Runs the warmup stage of training which is used to fill the agents memory.

	The agent uses it's rule-based policy to make actions. The agent's memory is filled as this runs.
	Loop terminates when the size of the memory is equal to WARMUP_MEM or when the memory buffer is full.

	"""

	print('Warmup Started...')
	total_step = 0
	while total_step < WARMUP_MEM and not dqn_agent.is_memory_full():
		# Reset episode
		user_action = episode_reset()
		done = False
		# Get initial state from state tracker
		state = state_tracker.get_state()
		turns=0
		while not done:
			next_user_action, next_state, reward, done, success = run_round(user_action, state, warmup=True)
			total_step += 1
			state = next_state
			user_action = next_user_action
			turns+=1
		print("Warmup episode {} out of {} with turns {}, reward {}, success {}\n".format(total_step,WARMUP_MEM,turns,reward,success))

	print('...Warmup Ended')

def train_run():
	"""
	Runs the loop that trains the agent.

	Trains the agent on the goal-oriented chatbot task. Training of the agent's neural network occurs every episode that
	TRAIN_FREQ is a multiple of. Terminates when the episode reaches MAX_EPISODES.

	"""

	print('Training Started...')
	episode = 0
	period_reward_total = 0
	period_turns = 0
	avg_reward_best = 0.0
	period_success_total = 0
	success_rate_best = 0.0
	while episode < MAX_EPISODES:
		user_action = episode_reset()
		episode += 1
		done = False
		state = state_tracker.get_state()
		while not done:
			next_user_action, next_state, reward, done, success = run_round(user_action, state, warmup=False)
			period_reward_total += reward
			state = next_state
			user_action = next_user_action
			period_turns+=1

		period_success_total += success

		if episode % 1000 == 0:
			avg_turns = period_turns / 1000
			print("Training episode {} of {} and turns {}\n".format(episode,MAX_EPISODES,avg_turns))
			period_turns=0

		# Train
		if episode % TRAIN_FREQ == 0:
			# Check success rate
			success_rate = period_success_total / TRAIN_FREQ
			avg_reward = period_reward_total / TRAIN_FREQ

			# Flush
			if success_rate >= success_rate_best and success_rate >= SUCCESS_RATE_THRESHOLD:
				dqn_agent.empty_memory()
			# Update current best success rate
			if success_rate > success_rate_best: #or avg_reward > avg_reward_best:
				print('Episode: {} NEW BEST SUCCESS RATE: {} NEW BEST Avg Reward: {}' .format(episode, success_rate, avg_reward))
				success_rate_best = success_rate
				avg_reward_best = avg_reward
				dqn_agent.save_weights()
			period_success_total = 0
			period_reward_total = 0
			# Copy
			dqn_agent.copy()
			# Train
			dqn_agent.train()

		if elderlyhelper.Parameters.END_EPSILON_DECAYING >= episode >= elderlyhelper.Parameters.START_EPSILON_DECAYING:
			elderlyhelper.Parameters.EPSILON_INIT -= elderlyhelper.Parameters.EPSILON_DECAY_VALUE
			#print("EPS",elderlyhelper.Parameters.EPSILON_INIT)
	print('...Training Ended on {} total episodes'.format(MAX_EPISODES))

def episode_reset():
	"""
	Resets the episode/conversation in the warmup and training loops.

	Called in warmup and train to reset the state tracker, user and agent. Also get's the initial user action.

	"""
	#print("NEW EPISODE...")
	#print('\n')
	elderlyhelper.DialogueUtils.get_goal()
	# First reset the state tracker
	state_tracker.reset()
	# Then pick an init user action
	user_action = user.reset()
	# Infuse with error (probabilistically alter slot names and also generate new ASR confidence values and slot similarity values)
	emc.reset()
	emc.infuse_error(user_action)
	# And update state tracker
	state_tracker.update_state_user(user_action)
	# Finally, reset agent
	dqn_agent.reset()

	return user_action

warmup_run()
train_run()
